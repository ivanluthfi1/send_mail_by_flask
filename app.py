from flask import Flask
from flask_mail import Mail, Message

app = Flask(__name__)

app.config['DEBUG'] = True
app.config['TESTING'] = False
app.config['MAIL_SERVER'] = 'localhost'  #setyour server here
app.config['MAIL_PORT'] = 587  #set your port here
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
#app.config['MAIL_DEBUG'] = True
app.config['MAIL_USERNAME']= 'ivanluthfi@testing.com'
app.config['MAIL_PASSWORD'] = 'P4ssword'
app.config['MAIL_DEFAULT_SENDER'] = 'ivanluthfi@testing.com'
app.config['MAIL_MAX_EMAILS'] = None
#app.config['MAIL_SUPPRESS_SEND'] = False
app.config['MAIL_ASCII_ATTACHMENTS'] = False

mail = Mail(app)

#mail = Mail()
#mail.init_app(app)

@app.route('/')
def index():
    msg = Message('Hey There', recipients=['sunuwibib@business-agent.info'])
    msg.html = '<b>This is a test email sent from Ivan\'s App. Do not reply. </b>'
    mail.send(msg)

    msg = Message(
        subject = '',
        recipients = [],
        body = '',
        html = '',
        sender = '',
        cc = [],
        bcc = [],
        reply_to = [],
        date = 'date',
    )
    return 'E-mail has been sent!'

if __name__ == '__main__':
    app.run()